'use strict';

angular.module('push-service', [])
    .provider('pushService', [function () {


        this.$get = ['$rootScope', '$http', function ($rootScope, $http) {
            var config;

            function registerPush(_config) {
                config = _config;

                var push = PushNotification.init(config);

                push.on('registration', function (data) {
                    updateServer(data.registrationId);
                });

                push.on('notification', function(data) {
                    console.log(data);

                    push.on('notification', function (data) {
                        console.log(data);
                        $rootScope.$broadcast('notification', {data: data});

                    });

                });

                push.on('error', function(e) {
                    console.log('Push error = ' + e)
                });

                if (config.useDefaultToast) {
                    $rootScope.$on('notification', function (event, data) {
                        window.plugins.toast.showLongTop(data.data.title + ': ' + data.data.message, function (a) {
                            console.log('toast success: ' + a)
                        }, function (b) {
                            console.log('toast error: ' + b)
                        })
                    })
                }

            }

            function updateServer(deviceToken) {
                register(config.platform, deviceToken).then(function (response) {   //api/subscribers/subscribe (POST) {deviceId}
                }, function (error) {
                    console.log("Register Push error: " + error);
                })
            }

            function getServerUrl (url, endpoint) {
                if (!endpoint) {
                    endpoint = '/registerPush';
                }

                if (url && endpoint) {
                    if (!(url[url.length-1] === '/') && !(endpoint[0] === '/')) {
                        return url + '/' + endpoint;
                    } else {
                        return url + endpoint;
                    }
                } else {
                    return '127.0.0.1/registerPush'
                }
            }


            function setConfig(_config) {
                config = _config;
            }

            function register(platform, regId) {
                var url = getServerUrl(config.serverUrl, config.serverEndpoint);

                return $http.post(url, {platform: config.platform, regId: regId});
            }


            return {
                registerPush: registerPush
            }


        }];

    }]);

