A mobile (iOS, Android) push notification handler for the client.
 
 ## Basic usage:
  * load module
  * Create a config object
  * call the registerPush(config)
 
 ### Example:
 ```javascript
    var config = {
       android: {
           senderID: "12345678910"
       },
       ios: {
           alert: "true",
           badge: "true",
           sound: "true"
       },
       serverUrl: 'http://127.0.0.1:2100',
       serverEndpoint: '/someServerRoute', // Default is '/registerPush'
       platform: 'ios',
       useDefaultToast: true // Displaying the notification when the user is with the app open
    };

    pushService.registerPush(config);
```
